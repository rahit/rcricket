#pragma once
#include "common.h"
#include "Graphics.h"


Graphics::Graphics(){
	SDL_Init( SDL_INIT_EVERYTHING ); 
    //Set up the screen
    this->screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );
    //Initialize SDL_ttf
    TTF_Init();
    //Set the window caption
    SDL_WM_SetCaption( "rCricket2013", NULL );
	this->textColor.b = 255; this->textColor.g = 255; this->textColor.r = 255;
}

SDL_Surface *Graphics::load_image( string filename )
{
	//The image that's loaded
	SDL_Surface* loadedImage = NULL;

	//The optimized surface that will be used
	SDL_Surface* optimizedImage = NULL;

	//Load the image
	loadedImage = IMG_Load( filename.c_str() );

	//If the image loaded
	if( loadedImage != NULL )
	{
		//Create an optimized surface
		optimizedImage = SDL_DisplayFormat( loadedImage );

		//Free the old surface
		SDL_FreeSurface( loadedImage );

		//If the surface was optimized
		if( optimizedImage != NULL )
		{
			//Color key surface
			//Reference : http://www.colorhexa.com/ca18df
			SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB( optimizedImage->format, 0xca, 0x18, 0xdf ) );
		}
	}
	//Return the optimized surface
	return optimizedImage;
}



void Graphics::apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip){
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;
	
    //Blit
    SDL_BlitSurface( source, clip, destination, &offset );
}


void Graphics::clean_up(){	
	//Freeing Surface
	for(int i = 0; i < this->location_flag; i++){
		SDL_FreeSurface(this->screenBackgrounds[i].location);    
	}  
	for(int i = 0; i < this->text_flag; i++){
		SDL_FreeSurface(this->screenMessages[i].text);
	}	
    //Quit SDL
    SDL_Quit();
}

Graphics::~Graphics(){
	//Freeing Surface
	for(int i = 0; i < this->location_flag; i++){
		if(this->screenBackgrounds[i].location){
			SDL_FreeSurface(this->screenBackgrounds[i].location);    
		}
	}  
	for(int i = 0; i < this->text_flag; i++){
		if(this->screenMessages[i].text){
			SDL_FreeSurface(this->screenMessages[i].text);
		}
	}	
}