#pragma once
#include "Graphics.h"
class Screen :
	public Graphics
{	
private:


protected:	


public:
	//The font that's going to be used
	TTF_Font *font;

	Screen(string, int);	
	bool load_background( int x = 0, int y = 0, string location = "resources/background.png", int w = 0, int h = 0 );
	bool load_message(int x = 0, int y = 0, string text = "" , int w = 0, int h = 0 );
	bool trigger_surface();
};

