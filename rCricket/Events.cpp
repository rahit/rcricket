#pragma once
#include "common.h"
#include "Graphics.h"
#include "Screen.h"
#include "Events.h"


Events::Events(){
	quit_flag = false;
}


void Events::batting_placement(Screen &field , int &placement){
	switch( this->event_key.key.keysym.sym ){
		case SDLK_UP:
			placement = placement*10 + upArrow ;
			break;
		case SDLK_RIGHT:
			placement = placement*10 + rightArrow ;
			break;
		case SDLK_DOWN:
			placement = placement*10 + downArrow ;
			break;
		case SDLK_LEFT:
			placement = placement*10 + leftArrow ;
			break;
		case SDLK_SPACE:
			placement = placement*10 + spaceBar ;
			break;
		default:
			break;
	}
}


Events::~Events()
{
}
