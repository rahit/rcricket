#pragma once
#include "common.h"
#include "Graphics.h"
#include "Screen.h"
#include "Events.h"
#include "Team.h"
#include "Game.h"
#include "GameEngine.h"


Game::Game(){	
	
}


void Game::initiate(){
	
	Screen welcome("resources/tahoma.ttf",50);
	welcome.textColor.b = 0;
	welcome.textColor.g = 165;
	welcome.textColor.r = 255;
	if(welcome.load_background(0,0,"resources/iccwc2012.jpg") && welcome.load_message(750,150,"rCricket")){
		welcome.trigger_surface();
		welcome.font = TTF_OpenFont( "resources/tahoma.ttf" , 20 );
		welcome.load_message(100,600," Pres any key to continue...");
		welcome.trigger_surface();
	}
	
	while (!eventHandler.quit_flag){
		while (SDL_PollEvent( &eventHandler.event_key ) ){
			if(eventHandler.event_key.type == SDL_KEYDOWN ||  eventHandler.event_key.type == SDL_QUIT ){
				eventHandler.quit_flag = true;				
			}
		}
	}
	eventHandler.quit_flag = false;

}


void Game::game_selection(){
	Screen team_selection("resources/tahoma.ttf",20);
	this->gamer_team = "";
	this->opponent_team = "";
	this->game_length = NULL;

	team_selection.load_message(10,0,"Choose your team");
	Team teams[TOTAL_TEAM] = {
						Team("Bangladesh"),
						Team("Australia")
					};
	for (int i = 0, temp = 50; i < TOTAL_TEAM; i++, temp += 100){
		team_selection.load_message(50,temp,teams[i].team_name);
		/*if(team_selection.load_message(850,temp,teams[i].team_name)){
			team_selection.trigger_surface();
		}*/
	}

	
	team_selection.trigger_surface();

	int selection_count = 0;
	while (!eventHandler.quit_flag){
		while (SDL_PollEvent( &eventHandler.event_key ) ){
			if(eventHandler.event_key.type == SDL_MOUSEBUTTONDOWN){
				int x = eventHandler.event_key.motion.x;
				int y = eventHandler.event_key.motion.y; 
				if(x < 500 &&  y < 600 && this->gamer_team == ""){
					for(int i = 0, temp = 50; i < TOTAL_TEAM; i++, temp += 100){
						if( x > 50 && y > temp && x < 200 && y < temp+50 ) {
							if(team_selection.load_background(20,temp+5, "resources/accept.png" )){
								this->gamer_team = teams[i].team_name;
								for (int i = 0, temp = 50; i < TOTAL_TEAM; i++, temp += 100){
									/*if(team_selection.load_message(50,temp,teams[i].team_name) ){
										team_selection.trigger_surface();
									}*/
									team_selection.load_message(800,0,"Choose your opponant");
									if(teams[i].team_name != this->gamer_team ){
										team_selection.load_message(850,temp,teams[i].team_name);
									}
								}
								break;
							}
						}
					}	
				}
				else if (this->gamer_team != "" && (x > 500 && y < 600 && this->opponent_team == "") ){
					for(int i = 0, temp = 50; i < TOTAL_TEAM; i++, temp += 100){
						if( x > 800 && y > temp && x < 1024 && y < temp+50 ) {
							if(team_selection.load_background(800,temp+5, "resources/accept.png" )){
								this->opponent_team = teams[i].team_name;
								team_selection.load_message(10,700,"Game Length : ");
								team_selection.load_message(200,700,"5 Over");
								team_selection.load_message(500,700,"2 Over");
								team_selection.load_message(800,700,"1 Over");
								break;
							}
						}
					}	
				}
				else if(this->opponent_team != "" && ( y > 650 && this->game_length == NULL)){
					for(int i = 0, temp = 200; i < 3; i++, temp += 300){
						if( x > temp && y > 700 && x < temp+50 && y < 750 ) {
							if(team_selection.load_background(temp-30,700, "resources/accept.png" )){
								if(i == 0)
									this->game_length = 5;
								else if(i == 1)
									this->game_length = 2;
								else if(i == 2)
									this->game_length = 1;
								break;
							}
						}
					}
				}
				
			team_selection.trigger_surface();
			}
			if(this->gamer_team != "" && this->opponent_team != "" && this->game_length != NULL){
				eventHandler.quit_flag = true;
			}
			if( eventHandler.event_key.type == SDL_QUIT ){
				eventHandler.quit_flag = true;
			}
		}
	}	
	eventHandler.quit_flag = false;
}


void Game::toss(){
	this->gamer_toss_selection = NULL;	
	Screen toss_screen("resources/tahoma.ttf",60);
	if(toss_screen.load_message(450,100,"Heads") ){
		toss_screen.trigger_surface();	}
	if(toss_screen.load_message(450,250,"Tails")){
		toss_screen.trigger_surface();
	}
	srand(time(NULL));
	bool gamerWonTheToss = false;
	while ( !eventHandler.quit_flag){
		while (SDL_PollEvent( &eventHandler.event_key ) ){			
			if(eventHandler.event_key.type == SDL_MOUSEBUTTONDOWN){
				int x = eventHandler.event_key.motion.x;
				int y = eventHandler.event_key.motion.y; 
					if( (x > 450 &&  y > 100 && x < 550 && y < 170)  || (x > 450 &&  y > 250 && x < 550 && y < 320 ) ){
						srand(time(NULL));
						gamerWonTheToss = rand()%2;
						if(gamerWonTheToss){
							this->heads_or_tails();
						}
						else{
							srand(time(NULL));
							this->gamer_toss_selection =  (rand()%2) + 1;
						}
						eventHandler.quit_flag = true;
					}
			}
			
			if( eventHandler.event_key.type == SDL_QUIT ){
				eventHandler.quit_flag = true;
			}
		}
	}
	if(this->gamer_toss_selection == Bat){
		this->current_batting_team = this->gamer_team;
		this->current_bowling_team = this->opponent_team;
	}
	else{
		this->current_batting_team = this->opponent_team;
		this->current_bowling_team = this->gamer_team;
	}
	eventHandler.quit_flag = false;

	Screen gamers_selection("resources/tahoma.ttf",16);		
	gamers_selection.load_message(10,10, "Your Team : " + this->gamer_team );
	gamers_selection.load_message(850,50, "Your Opponent : " + this->opponent_team );
	gamers_selection.load_message(300,90, "Game Length : " + StringOf(this->game_length) + " Overs" );
	gamers_selection.load_message(300,140, "Toss : "  );
	gamers_selection.load_message(350,140, (this->gamer_toss_selection == 1) ? "Bat" : "Bowl" );
	gamers_selection.trigger_surface();
	gamers_selection.font = TTF_OpenFont( "resources/tahoma.ttf" , 12 );
	gamers_selection.load_message(750,700, "Press Any key to Start playing"  );
	Team gamer_team_detail(this->gamer_team);
	Team opponent_team_detail(this->opponent_team);
	for (int i = 0, temp = 50; i < 11; i++, temp += 50){
		gamers_selection.load_message(20,temp,gamer_team_detail.players[i].player_name);
		gamers_selection.load_message(850,temp,opponent_team_detail.players[i].player_name);
	}
	gamers_selection.trigger_surface();

	while (!eventHandler.quit_flag){
		while (SDL_PollEvent( &eventHandler.event_key ) ){
			if(eventHandler.event_key.type == SDL_KEYDOWN ||  eventHandler.event_key.type == SDL_QUIT ){
				eventHandler.quit_flag = true;				
			}
		}
	}
	eventHandler.quit_flag = false;
}

void Game::heads_or_tails(){	
	Screen toss_selection("resources/tahoma.ttf",20);		
	
	toss_selection.load_message(250,10,"You've won the Toss. Select your choice..");
	toss_selection.trigger_surface();
	toss_selection.font = TTF_OpenFont( "resources/tahoma.ttf" , 60 );
	toss_selection.load_message(450,100,"Bowl");
	toss_selection.load_message(450,250,"Bat");
	toss_selection.trigger_surface();

	while ((this->gamer_toss_selection != Bat || this->gamer_toss_selection != Bowl ) && !eventHandler.quit_flag){
		while (SDL_PollEvent( &eventHandler.event_key ) ){			
			if(eventHandler.event_key.type == SDL_MOUSEBUTTONDOWN){
				int x = eventHandler.event_key.motion.x;
				int y = eventHandler.event_key.motion.y; 
				if(x > 450 &&  y > 100 && x < 550 && y < 170){
					this->gamer_toss_selection = Bowl;
				}
				else if (x > 450 &&  y > 250 && x < 550 && y < 320) {
					this->gamer_toss_selection = Bat;	
				}
				eventHandler.quit_flag = true;
			}
			if( eventHandler.event_key.type == SDL_QUIT ){
				this->gamer_toss_selection = Bat;
				eventHandler.quit_flag = true;
			}
		}
	}
	eventHandler.quit_flag = false;
}


void Game::first_innings(Game &rCricket){
	//GameEngine game_engine();
	switch (this->gamer_toss_selection)	{
	case Bowl:		
		this->total_run = 0;
		this->total_ball = 0;
		this->wicket = 0;
		this->current_bowler = 10;
		this->current_batsman = 0;
		while (this->total_ball < this->game_length*6 && this->wicket < 10){
		//while (this->total_ball < 6){
			GameEngine game_engine(rCricket);
			srand(time(NULL));
			if(!(this->total_ball%6))
				this->current_bowler--;
			game_engine.bowl();
			this->total_ball++;
		}
		break;
	case Bat:				
		this->total_run = 0;
		this->total_ball = 0;
		this->wicket = 0;
		this->current_bowler = 10;
		this->current_batsman = 0;
		while (this->total_ball < this->game_length*6 && this->wicket < 10){
		//while (this->total_ball < 6){
			GameEngine game_engine(rCricket);
			srand(time(NULL));
			if(!(this->total_ball%6))
				this->current_bowler--;
			game_engine.bat(rand()%9);
			this->total_ball++;
		}
		break;

	default:
		break;
	}
}


void Game::second_innings(Game &rCricket){
	//GameEngine game_engine(); 	
	
		string temp_team = this->current_batting_team;
		this->current_batting_team = this->current_bowling_team;
		this->current_bowling_team = temp_team;
		this->target = this->total_run + 1;
		
	switch (this->gamer_toss_selection)	{
	case Bat:		
		this->total_run = 0;
		this->total_ball = 0;
		this->wicket = 0;
		this->current_bowler = 10;
		this->current_batsman = 0;
		while (this->total_ball < this->game_length*6 && this->wicket < 10 && this->total_run <= this->target ){
		//while (this->total_ball < 6){
			GameEngine game_engine(rCricket);
			srand(time(NULL));
			if(!(this->total_ball%6))
				this->current_bowler--;
			game_engine.bowl();
			this->total_ball++;
		}
		break;
	case Bowl:				
		this->total_run = 0;
		this->total_ball = 0;
		this->wicket = 0;
		this->current_bowler = 10;
		this->current_batsman = 0;
		while (total_ball < this->game_length*6 && this->wicket < 10 && this->total_run <= this->target ){
		//while (this->total_ball < 6){
			GameEngine game_engine(rCricket);
			srand(time(NULL));
			if(!(this->total_ball%6))
				this->current_bowler--;
			game_engine.bat(rand()%9);
			this->total_ball++;
		}
		break;
	default:
		break;
	}
}


void Game::finish_screen(){
	Screen resultScreen("resources/tahoma.ttf",25);
	if(this->gamer_toss_selection == Bat && this->total_run < this->target ){
		resultScreen.load_message(300,100,"CONGRATULATIONS !!! You WON :D");
		resultScreen.load_message(250,350,this->gamer_team.c_str());
		resultScreen.load_message(400,350,"Wins by ");
		resultScreen.load_message(500,350,StringOf((this->target--)-this->total_run));
		resultScreen.load_message(550,350," runs");
	}
	else if(this->gamer_toss_selection == Bowl && this->total_run > this->target ){		
		resultScreen.load_message(300,100,"CONGRATULATIONS !!! You WON :D");
		resultScreen.load_message(250,350,this->gamer_team.c_str());
		resultScreen.load_message(400,350," wins by ");
		resultScreen.load_message(500,350,StringOf(10-this->wicket));
		resultScreen.load_message(550,350," wickets");
	}
	else if(this->total_run == this->target){
		resultScreen.load_message(320,100,"WHAT A MATCH......!!!!");
		resultScreen.load_message(400,350,"Match DRAWN");
	}
	else{
		resultScreen.load_message(250,100,"OOOooppss !! You lose. Better luck next time");
		if(this->gamer_toss_selection == Bat ){
			resultScreen.load_message(250,350,this->opponent_team.c_str());
			resultScreen.load_message(400,350," wins by ");
			resultScreen.load_message(500,350,StringOf( 10 - this->wicket));
			resultScreen.load_message(550,350," wickets");
		}
		else if(this->gamer_toss_selection == Bowl ){
			resultScreen.load_message(250,350,this->gamer_team.c_str());
			resultScreen.load_message(400,350,"Wins by ");
			resultScreen.load_message(500,350,StringOf((this->target--) - this->total_run ));
			resultScreen.load_message(550,350," run");
		}
	}
	resultScreen.trigger_surface();
    //While the user hasn't quit
    while( eventHandler.quit_flag == false ){
        //While there's events to handle
        while( SDL_PollEvent(&eventHandler.event_key ) )
        {
            //If the user has Xed out the window
            if( eventHandler.event_key.type == SDL_QUIT )
            {
                //Quit the program
                eventHandler.quit_flag  = true;
            }
        }
    }
}


Game::~Game(){

}
