#pragma once
#include "Events.h"

class Game
	: public Events
{
private:
	void heads_or_tails();

protected:	


public:
	enum coin{Bat = 1, Bowl = 2};
	Events eventHandler;
	string gamer_team;
	string opponent_team;
	int game_length;
	int gamer_toss_selection;
	int total_run;
	int total_ball;
	int wicket;
	int current_bowler;
	int current_batsman;
	int target;
	string current_batting_team;
	string current_bowling_team;
	Game();
	void initiate();
	void game_selection();
	void toss();
	void first_innings(Game &);
	void second_innings(Game &);
	void finish_screen();
	~Game();
};

