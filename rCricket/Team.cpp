#pragma once
#include "common.h"
#include "Team.h"


Team::Team(string team_name){
	enum player_types{Batsman = 1, Allrounder = 2, Bowler = 3, Keeper = 4};
	if(team_name == "Bangladesh"){
		this->team_name = team_name;
		this->players[0].player_name = "Tamim Iqbal";
		this->players[1].player_name = "Anamul Haque";
		this->players[2].player_name = "Nasir Hossain";
		this->players[3].player_name = "Shakib Al Hassan";
		this->players[4].player_name = "Mushfiqur Rahim";
		this->players[5].player_name = "Mahmudullah Riad";
		this->players[6].player_name = "Naeem Islam";
		this->players[7].player_name = "Shohag Gazi";
		this->players[8].player_name = "Mashrafee Mortaza";
		this->players[9].player_name = "Shafiul Islam";
		this->players[10].player_name = "Abdur Razzak";
		
		this->players[0].player_type = Batsman;
		this->players[1].player_type = Batsman;
		this->players[2].player_type = Allrounder;
		this->players[3].player_type = Allrounder;
		this->players[4].player_type = Keeper;
		this->players[5].player_type = Allrounder;
		this->players[6].player_type = Allrounder;
		this->players[7].player_type = Bowler;
		this->players[8].player_type = Bowler;
		this->players[9].player_type = Bowler;
		this->players[10].player_type = Bowler;
		
		this->players[0].player_rating = 9.5;
		this->players[1].player_rating = 7.5;
		this->players[2].player_rating = 9;
		this->players[3].player_rating = 9.9;
		this->players[4].player_rating = 8.5;
		this->players[5].player_rating = 8.2;
		this->players[6].player_rating = 8.1;
		this->players[7].player_rating = 9;
		this->players[8].player_rating = 8;
		this->players[9].player_rating = 8.8;
		this->players[10].player_rating = 8;
	}
	else if(team_name == "Australia"){
		this->team_name = team_name;
		this->players[0].player_name = "a";
		this->players[1].player_name = "b";
		this->players[2].player_name = "c";
		this->players[3].player_name = "d";
		this->players[4].player_name = "e";
		this->players[5].player_name = "f";
		this->players[6].player_name = "g";
		this->players[7].player_name = "h";
		this->players[8].player_name = "i";
		this->players[9].player_name = "j";
		this->players[10].player_name = "k";
		
		this->players[0].player_type = Batsman;
		this->players[1].player_type = Allrounder;
		this->players[2].player_type = Keeper;
		this->players[3].player_type = Batsman;
		this->players[4].player_type = Batsman;
		this->players[5].player_type = Allrounder;
		this->players[6].player_type = Allrounder;
		this->players[7].player_type = Bowler;
		this->players[8].player_type = Bowler;
		this->players[9].player_type = Bowler;
		this->players[10].player_type = Bowler;
		
		this->players[0].player_rating = 9.5;
		this->players[1].player_rating = 9;
		this->players[2].player_rating = 9;
		this->players[3].player_rating = 8.5;
		this->players[4].player_rating = 8.2;
		this->players[5].player_rating = 8.8;
		this->players[6].player_rating = 8.9;
		this->players[7].player_rating = 9;
		this->players[8].player_rating = 8.8;
		this->players[9].player_rating = 8.9;
		this->players[10].player_rating = 8.9;
	}
}


Team::~Team(void)
{
}
