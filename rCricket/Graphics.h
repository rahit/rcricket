#pragma once
class Graphics
{
private:


protected:	


public:	
	//The color of the font
	SDL_Color textColor;

	//The surfaces
	background screenBackgrounds[MAX_SCREEN_ELEMENT];	
	message screenMessages[MAX_SCREEN_ELEMENT];
	int location_flag ;
	int text_flag ;	

	SDL_Surface *screen;
	//The event structure
	SDL_Event event;

	Graphics();
	~Graphics();
	SDL_Surface *load_image(string);
	void apply_surface( int, int, SDL_Surface*, SDL_Surface*, SDL_Rect* clip = NULL );
	void clean_up();
};

