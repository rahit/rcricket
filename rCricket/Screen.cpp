#pragma once
#include "common.h"
#include "Screen.h"



Screen::Screen(string font_file = "resources/BuxtonSketch.ttf", int font_size = 20){
	for(int i = 0; i < MAX_SCREEN_ELEMENT; i++ ){
		this->screenBackgrounds[i].location = NULL;
		this->screenMessages[i].text = NULL;
	}
	this->location_flag = 0;
	this->text_flag = 0;
	this->font = TTF_OpenFont( font_file.c_str(), font_size );
}


bool Screen::load_background( int x , int y , string location , int w , int h ){
    //Load the background image
	this->screenBackgrounds[this->location_flag].location = this->load_image( location.c_str());
	this->screenBackgrounds[this->location_flag].location_x = x;
	this->screenBackgrounds[this->location_flag].location_y = y;
	this->screenBackgrounds[this->location_flag].location_w = w;
	this->screenBackgrounds[this->location_flag].location_h = h;
	this->location_flag++;
    return true;
}


bool Screen::load_message( int x , int y , string text , int w , int h ){
	//Load the background image
	this->screenMessages[this->text_flag].text = TTF_RenderText_Solid( this->font, text.c_str(), this->textColor );
	this->screenMessages[this->text_flag].text_x = x;
	this->screenMessages[this->text_flag].text_y = y;
	this->screenMessages[this->text_flag].text_w = w;
	this->screenMessages[this->text_flag].text_h = h;
	this->text_flag++;
    return true;
}


bool Screen::trigger_surface(){
	for(int i = 0; i < this->location_flag; i++){
		if(this->screenBackgrounds[i].location != NULL){
			this->apply_surface(this->screenBackgrounds[i].location_x, this->screenBackgrounds[i].location_y, this->screenBackgrounds[i].location, this->screen, NULL );
		}
	}
	for(int i = 0; i < this->text_flag; i++){
		if(this->screenMessages[i].text != NULL){
			this->apply_surface(this->screenMessages[i].text_x, this->screenMessages[i].text_y, this->screenMessages[i].text, this->screen,  NULL );
		}
	}
	
	SDL_Flip( this->screen );
	return true;
}