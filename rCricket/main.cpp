#include "common.h"
#include "Graphics.h"
#include "Screen.h"
#include "Events.h"
#include "Team.h"
#include "Game.h"
#include "GameEngine.h"


int main( int argc, char* args[] )
{
    //Quit flag
    bool quit = false;

	Game rCricket;
	rCricket.initiate();
	rCricket.game_selection();
	rCricket.toss();
	rCricket.first_innings(rCricket);
	rCricket.second_innings(rCricket);
	rCricket.finish_screen();


	
    //Quit SDL_ttf
    TTF_Quit();
    //Quit SDL
    SDL_Quit();
    return 0;
}
