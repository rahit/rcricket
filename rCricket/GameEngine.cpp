#pragma once
#include "common.h"
#include "Graphics.h"
#include "Screen.h"
#include "Events.h"
#include "Team.h"
#include "Game.h"
#include "GameEngine.h"


GameEngine::GameEngine(Game &rCricket){
	this->rCricket = &rCricket;
}


void GameEngine::bowl(){
	Screen field("resources/tahoma.ttf",10);	
	field.textColor.b = 0;
	field.textColor.g = 0;
	field.textColor.r = 0;
	Events eventHandler;
	int pitch_x = 0,pitch_y = 0;
	bool shot_placement[6] = {false};	// See key reference at bottom for detail

	SDL_FillRect( field.screen, &field.screen->clip_rect, SDL_MapRGB( field.screen->format, 0x66, 0xed, 0x56 ) );
	field.load_background(0,0,"resources/field.png");
	field.load_background(420,300,"resources/pitch.png");
	field.load_background(250,300,"resources/ball.png");
	field.font = TTF_OpenFont( "resources/tahoma.ttf" , 20 );
	field.load_message(10,730, rCricket->current_batting_team);
	field.load_message(150,730, StringOf(rCricket->total_run));
	field.load_message(180,730, " / ");
	field.load_message(200,730, StringOf(rCricket->wicket));
	field.load_message(300,730, "Overs : ");
	field.load_message(400, 730, StringOf(rCricket->total_ball/6) );
	field.load_message(415, 730, "." );
	field.load_message(420, 730, StringOf(rCricket->total_ball%6) );
	this->fielding_position(field, Aggressive);
	field.trigger_surface();

	int ball_type = 0;
	while ( !eventHandler.quit_flag){
		while (SDL_PollEvent( &eventHandler.event_key ) ){			
			if(eventHandler.event_key.type == SDL_MOUSEBUTTONDOWN){
				int x = eventHandler.event_key.motion.x;
				int y = eventHandler.event_key.motion.y; 
				if(x >= 630 && y >= 308 && y < 315 ){
					ball_type = fullOff;
					eventHandler.quit_flag = true;
				}
				else if(x >= 630 && y >= 315 && y < 328 ){
					ball_type = fullStraight;
					eventHandler.quit_flag = true;
				}
				else if (x >= 630 && y >= 328 && y < 338 ){
					ball_type = fullLeg;
					eventHandler.quit_flag = true;
				}
				else if(x >= 600 && x < 630 && y >= 308 && y < 315 ){
					ball_type = goodOff;
					eventHandler.quit_flag = true;
				}
				else if (x >= 600 && x < 630 && y >= 315 && y < 328 ){
					ball_type = goodStraight;
					eventHandler.quit_flag = true;
				}
				else if(x >= 600 && x < 630 && y >= 328 && y < 338 ){
					ball_type = goodLeg;
					eventHandler.quit_flag = true;
				}
				else if (x < 600 && y >= 308 && y < 315 ){
					ball_type = shortOff;
					eventHandler.quit_flag = true;
				}
				else if(x < 600 && y >= 315 && y < 328 ){
					ball_type = shortStraight;
					eventHandler.quit_flag = true;
				}
				else if (x < 600 && y >= 328 && y < 338 ){
					ball_type = shortLeg;
					eventHandler.quit_flag = true;
				}
				else if(y >= 338 ){
					ball_type = legWide;
					eventHandler.quit_flag = true;
				}
				else if(y < 308 ){
					ball_type = offWide;
					eventHandler.quit_flag = true;
				}
				
			}
			
			if( eventHandler.event_key.type == SDL_QUIT ){
				eventHandler.quit_flag = true;
			}
		}
	}

	this->bat(ball_type);
}


void GameEngine::bat(int ball_type){
	Screen field("resources/tahoma.ttf",10);	
	field.textColor.b = 0;
	field.textColor.g = 0;
	field.textColor.r = 0;
	Events eventHandler;
	int pitch_x = 0,pitch_y = 0;
	bool shot_placement[6] = {false};	// See key reference at bottom for detail

	SDL_FillRect( field.screen, &field.screen->clip_rect, SDL_MapRGB( field.screen->format, 0x66, 0xed, 0x56 ) );
	field.load_background(0,0,"resources/field.png");
	field.load_background(420,300,"resources/pitch_2.png");
	field.load_background(250,300,"resources/ball.png");
	field.font = TTF_OpenFont( "resources/tahoma.ttf" , 20 );
	field.load_message(10,730, rCricket->current_batting_team);
	field.load_message(150,730, StringOf(rCricket->total_run));
	field.load_message(180,730, " / ");
	field.load_message(200,730, StringOf(rCricket->wicket));
	field.load_message(300,730, "Overs : ");
	field.load_message(400, 730, StringOf(rCricket->total_ball/6) );
	field.load_message(415, 730, "." );
	field.load_message(420, 730, StringOf(rCricket->total_ball%6) );
	this->fielding_position(field, Aggressive);
	field.load_background(650,315,"resources/batsman_pre.png");
	field.load_background(432,335,"resources/batsman_pre.png");
	field.load_background(665,327,"resources/stamp_2.png");
	field.trigger_surface();

	switch (ball_type){
	case fullOff:
		pitch_x = 650;
		pitch_y = 310;
		break;

	case fullStraight:
		pitch_x = 650;
		pitch_y = 325;
		break;

	case fullLeg:
		pitch_x = 650;
		pitch_y = 330;
		break;

	case goodOff:
		pitch_x = 615;
		pitch_y = 310;
		break;

	case goodStraight:
		pitch_x = 615;
		pitch_y = 320;
		break;

	case goodLeg:
		pitch_x = 615;
		pitch_y = 330;
		break;

	case shortOff:
		pitch_x = 575;
		pitch_y = 310;
		break;

	case shortStraight:
		pitch_x = 575;
		pitch_y = 320;
		break;

	case shortLeg:
		pitch_x = 575;
		pitch_y = 330;
		break;

	case legWide:
		pitch_x = 630;
		pitch_y = 340;
		break;

	case offWide:
		pitch_x = 630;
		pitch_y = 305;
		break;

	default:
		break;
	}
	
	field.font = TTF_OpenFont( "resources/tahoma.ttf" , 5 );
	field.load_message(pitch_x,pitch_y,"O");
	field.trigger_surface();
	field.font = TTF_OpenFont( "resources/tahoma.ttf" , 14 );
	int placement = 0;
	while (!eventHandler.quit_flag){
		if(rCricket->current_batting_team == rCricket->gamer_team){
			while (SDL_PollEvent( &eventHandler.event_key ) ){
				if(eventHandler.event_key.type == SDL_KEYDOWN ){
					eventHandler.batting_placement(field,placement);
				}
				if(eventHandler.event_key.type == SDL_QUIT ){
					eventHandler.quit_flag = true;				
				}
			}
		}
		if(field.screenBackgrounds[2].location_x < 430){
			field.screenBackgrounds[2].location_x++;
			field.screenBackgrounds[3].location_x++;
		}
		else{
			if((field.screenBackgrounds[2].location_x < pitch_x || field.screenBackgrounds[2].location_y < pitch_y)){
				if (field.screenBackgrounds[2].location_x < pitch_x){
					field.screenBackgrounds[2].location_x++;
				}
				if (field.screenBackgrounds[2].location_y < pitch_y){
					field.screenBackgrounds[2].location_y++;
				}
				else if (field.screenBackgrounds[2].location_y > pitch_y){
					field.screenBackgrounds[2].location_y--;
				}
			}
			else{
				if (field.screenBackgrounds[2].location_x <= 660){
					field.screenBackgrounds[2].location_x++;
				}
				if((field.screenBackgrounds[2].location_x >= 660)){
					eventHandler.quit_flag = true;				
				}
			}
		}
		field.trigger_surface();
	}
			eventHandler.quit_flag = false;				

	/*
	int x = 505;
	int y = 365;
	int a = 480;
	int b = 350;
	for(int i = 0; i < 1024 ; i += 50){
		for(int j = 0; j < 768 ; j += 50){
			string temp = (StringOf(i)+StringOf(j));
			temp = StringOf( (powf((i-x),2)/(a * a)) +  (powf((j-y),2)/(b*b)) );
			if( (powf((i-x),2)/(a * a)) +  (powf((j-y),2)/(b*b)) == 1 ){
				field.load_message(i,j,temp);
				field.trigger_surface();
			}
		}
	}*/

	if(rCricket->current_batting_team != rCricket->gamer_team){
		srand(time(NULL));
		placement = rand()%5+1;
	}

	placement = placement%1000;
	int temp = 0;	
	do{
		temp = placement%10;
		placement = placement/10;
		shot_placement[temp] = true;
	} while (placement > 10);
	
	
	srand(time(NULL));
	int ball_destination_x = rand()%1024 ;//= 660;
	int ball_destination_y = rand()%768 ;//= 330;
	int the_shot = this->make_the_shot(ball_type,shot_placement);
	if(the_shot == run || the_shot == catchOut){
		//int major_axis_length_in = 100, minor_axis_length_in = 50;
		int center_x = 505, center_y = 365, major_axis_length = 475, minor_axis_length = 345;
		while((powf((ball_destination_x - center_x),2)/( major_axis_length * major_axis_length)) +  (powf((ball_destination_y - center_y),2)/(minor_axis_length*minor_axis_length)) > 1 /*&& (powf((ball_destination_x - center_x),2)/( major_axis_length_in * major_axis_length_in)) +  (powf((ball_destination_y - center_y),2)/(minor_axis_length_in*minor_axis_length_in)) <= 1 */){
			if(shot_placement[leftArrow]){
				ball_destination_x = rand()%500+20;
			}
			else{
				ball_destination_x = rand()%1000+20;
			}
			if(shot_placement[upArrow]){
				ball_destination_y = rand()%320+20;
			}
			else if(shot_placement[downArrow]){
				ball_destination_y = rand()%740+320;
			}
			else{
				ball_destination_y = rand();
			}
			ball_destination_x = 660 - abs(325 - ball_destination_y) ;
		}
		if(the_shot == catchOut){
			rCricket->wicket++;
			rCricket->current_batsman++;
			field.load_message(620, 742, "In the Air.....AND GONE.." );
		}
	}
	else if(the_shot == leaveBall || the_shot == wideBall){		
		ball_destination_x = 750;
		ball_destination_y = 333;
		if(the_shot == wideBall){				
			rCricket->total_run++;
			rCricket->total_ball--;
			field.load_message(620, 742, "<-------wide ball--------->" );
		}
	}
	else if(the_shot == boldOut){
		ball_destination_x = 670;
		ball_destination_y = 333;
		rCricket->wicket++;
		rCricket->current_batsman++;
		string temp_broken_stamp_location = "resources/stamp_broken.png";	
		field.screenBackgrounds[16].location = field.load_image( temp_broken_stamp_location.c_str()) ;
			field.load_message(620, 742, "BOLDEN !!! What a cracking delevery" );
	}
	else if(the_shot == fourRun || the_shot == sixRun){
		int center_x = 505, center_y = 365, major_axis_length = 482, minor_axis_length = 352;
		while((powf((ball_destination_x - center_x),2)/( major_axis_length * major_axis_length)) +  (powf((ball_destination_y - center_y),2)/(minor_axis_length*minor_axis_length)) <= 1 ){
			if(shot_placement[upArrow]){
				ball_destination_y = rand()%320+1;
			}
			else if(shot_placement[downArrow]){
				ball_destination_y = rand()%770+320;
			}
			else{					
				ball_destination_y = rand()%770+1;
			}

			ball_destination_x = rand()%1024+1;

			
			//ball_destination_x = 660 - abs(325 - ball_destination_y) ;
		}
		if(the_shot == fourRun){				
			rCricket->total_run += 4;
			field.load_message(620, 742, "Decent Shot..... Four Runs" );
		}
		else if(the_shot == sixRun){			
			rCricket->total_run += 6;
			field.load_message(620, 742, "GREAT SHOT...IT's GONE ALL THE WAY....SIX" );
		}
	}
	else if(the_shot == dotBall){
		int center_x = 505, center_y = 365, major_axis_length = 150, minor_axis_length = 80;
		while((powf((ball_destination_x - center_x),2)/( major_axis_length * major_axis_length)) +  (powf((ball_destination_y - center_y),2)/(minor_axis_length*minor_axis_length)) > 1  ){
			
			ball_destination_x = rand()%700+450;
			if(shot_placement[upArrow]){
				ball_destination_y = rand()%320+250;
			}
			else if(shot_placement[downArrow]){
				ball_destination_y = rand()%320+400;
			}
			else{
				ball_destination_y = rand()%400+250;
			}
						
			ball_destination_x = 660 - abs(325 - ball_destination_y) ;
		}
	}
	field.trigger_surface();


	int fielder_index = this->select_fielder(field,ball_destination_x,ball_destination_y);
	int fielder_destination_x = ball_destination_x, fielder_destination_y = ball_destination_y;
	bool go_for_run = false;
	int current_end_of_runner = 1;
	int run_count = 0;
	string temp_batsman_location = "resources/batsman_post.png";	
	field.screenBackgrounds[14].location = field.load_image( temp_batsman_location.c_str()) ;
	while (!eventHandler.quit_flag){
		if(the_shot == run || the_shot == dotBall ){
			while (SDL_PollEvent( &eventHandler.event_key ) ){
				if(rCricket->current_batting_team == rCricket->gamer_team){
					/*if(eventHandler.event_key.type == SDLK_RCTRL ){
						go_for_run = false;
					}
					else if(eventHandler.event_key.type == SDLK_RETURN){
						go_for_run = true;
					}*/
					
					if(eventHandler.event_key.type == SDL_MOUSEBUTTONDOWN ){
						go_for_run = true;
					}
					if(eventHandler.event_key.type == SDL_QUIT ){
						eventHandler.quit_flag = true;				
					}
				}
				else{
					if(sqrtf(powf((ball_destination_x - 660),2) + powf((ball_destination_y - 315),2)) < abs(432 - 650)){
						go_for_run = true;
					}
				}

			}

			if(go_for_run){
				if(current_end_of_runner == 1){
					field.screenBackgrounds[14].location_x--;
					field.screenBackgrounds[15].location_x++;
					if(field.screenBackgrounds[14].location_x == 432){
						go_for_run = false;
						current_end_of_runner = 2;
						run_count++;
					}
				}
				else{
					field.screenBackgrounds[14].location_x++;
					field.screenBackgrounds[15].location_x--;
					if(field.screenBackgrounds[14].location_x == 650){
						go_for_run = false;
						current_end_of_runner = 1;
						run_count++;
					}
				}
			}
			else if (!go_for_run && field.screenBackgrounds[14].location_x != 650 && field.screenBackgrounds[14].location_x != 432){
				if(current_end_of_runner == 1){
					field.screenBackgrounds[14].location_x++;
					field.screenBackgrounds[15].location_x--;
				}
				else{
					field.screenBackgrounds[14].location_x--;
					field.screenBackgrounds[15].location_x++;
				}
			}

		}


		if (field.screenBackgrounds[2].location_x < ball_destination_x){
			field.screenBackgrounds[2].location_x++ ;
		}
		else if (field.screenBackgrounds[2].location_x > ball_destination_x){
			field.screenBackgrounds[2].location_x-- ;
		}
		if (field.screenBackgrounds[2].location_y < ball_destination_y){
			field.screenBackgrounds[2].location_y++ ;
		}
		else if (field.screenBackgrounds[2].location_y > ball_destination_y){
			field.screenBackgrounds[2].location_y-- ;
		}

		if (field.screenBackgrounds[fielder_index].location_x < fielder_destination_x){
			field.screenBackgrounds[fielder_index].location_x++ ;
		}
		else if (field.screenBackgrounds[fielder_index].location_x > fielder_destination_x){
			field.screenBackgrounds[fielder_index].location_x-- ;
		}
		if (field.screenBackgrounds[fielder_index].location_y < fielder_destination_y){
			field.screenBackgrounds[fielder_index].location_y++ ;
		}
		else if (field.screenBackgrounds[fielder_index].location_y > fielder_destination_y){
			field.screenBackgrounds[fielder_index].location_y-- ;
		}

		SDL_Delay(5);
		field.trigger_surface();
		if((field.screenBackgrounds[2].location_x == ball_destination_x && field.screenBackgrounds[2].location_y == ball_destination_y) && (field.screenBackgrounds[fielder_index].location_x == fielder_destination_x && field.screenBackgrounds[fielder_index].location_y == fielder_destination_y)){
			//eventHandler.quit_flag = true;
			if(the_shot == run || the_shot == dotBall){
				if(sqrtf(powf((ball_destination_x - 660),2) + powf((ball_destination_y - 320),2)) < sqrtf(powf((ball_destination_x - 420),2) + powf((ball_destination_y - 320),2)) ){
					ball_destination_x = 660;
					ball_destination_y = 320;
				}
				else{
					ball_destination_x = 420;
					ball_destination_y = 320;
				}

			}
			else{
				eventHandler.quit_flag = true;
			}
		}
		if((field.screenBackgrounds[2].location_x == 420 && field.screenBackgrounds[2].location_y == 320) || (field.screenBackgrounds[2].location_x == 660 && field.screenBackgrounds[2].location_y == 320) ){
			if(((field.screenBackgrounds[2].location_x == 420 && field.screenBackgrounds[2].location_y == 320) || (field.screenBackgrounds[2].location_x == 660 && field.screenBackgrounds[2].location_y == 320)) && (field.screenBackgrounds[14].location_x != 650 || field.screenBackgrounds[14].location_x != 432)){
				rCricket->wicket++;
				rCricket->current_batsman++;
			}
			eventHandler.quit_flag = true;				
		}
	}


	rCricket->total_run += run_count;
	
	eventHandler.quit_flag = false;				
	SDL_Delay(1000);
}


void GameEngine::fielding_position(Screen &field, int type){
	switch (type){
	case Aggressive:		
		field.load_background(250,300,"resources/fielder.png"); // this is the bowler with index 3
		field.load_background(750,310,"resources/fielder.png"); // this is wicketkeeper with index 4
		field.load_background(400,180,"resources/fielder.png");
		field.load_background(500,170,"resources/fielder.png");
		field.load_background(600,180,"resources/fielder.png");
		field.load_background(350,480,"resources/fielder.png");
		field.load_background(520,490,"resources/fielder.png");
		field.load_background(650,480,"resources/fielder.png");
		field.load_background(730,270,"resources/fielder.png");
		field.load_background(720,260,"resources/fielder.png");
		field.load_background(120,200,"resources/fielder.png"); // last fielder with index 13
		field.trigger_surface();
	case Moderate:


	case Defensive:


	default:
		break;
	}
}


int GameEngine::select_fielder(Screen &field,int ball_destination_x, int ball_destination_y){
	int fielder_index = 3, temp = 0, distance = ( sqrt(pow((abs(field.screenBackgrounds[3].location_x - ball_destination_x)),2) +pow((abs(field.screenBackgrounds[3].location_y - ball_destination_y)),2)) ) ;
	for(int i = 3; i< 14; i++){
		temp = ( sqrt(pow((abs(field.screenBackgrounds[i].location_x - ball_destination_x)),2) + pow((abs(field.screenBackgrounds[i].location_y - ball_destination_y)),2)) );
		if( temp < distance ){
			distance = temp;
			fielder_index = i;
		}
	}
	return fielder_index;
}




int GameEngine::make_the_shot(int balltype, bool shot_placement[]){
	bool event_tracker[9] = {false};
	
	if(shot_placement[0] || (!shot_placement[leftArrow] && !shot_placement[upArrow] && !shot_placement[rightArrow] && !shot_placement[downArrow] && !shot_placement[spaceBar])){
		event_tracker[leaveBall] = true;
	}
	else if( (balltype == fullOff || balltype == goodOff || balltype == shortOff) && shot_placement[downArrow] ){
		event_tracker[catchOut] = true;
		event_tracker[run] = true;
		event_tracker[fourRun] = true;		
		event_tracker[sixRun] = true;
	}
	else if(balltype == fullOff && (shot_placement[leftArrow])){
		event_tracker[catchOut] = true;
	}
	else if( (balltype == fullOff || balltype == goodOff || balltype == shortOff)
			&& (shot_placement[leftArrow] || shot_placement[downArrow] || shot_placement[rightArrow] 
				|| (shot_placement[upArrow] && shot_placement[leftArrow]) 
				|| (shot_placement[leftArrow] && shot_placement[downArrow])
				|| (shot_placement[rightArrow] && shot_placement[downArrow])
				)
		){
		event_tracker[dotBall] = true;
	}
	else if ( (balltype == shortLeg || balltype == shortStraight || balltype == fullLeg)
			&& (shot_placement[downArrow] || (shot_placement[downArrow] && shot_placement[rightArrow] ) 
			)
		){
		event_tracker[catchOut] = true;
		event_tracker[fourRun] = true;		
		event_tracker[sixRun] = true;
	}
	else if ( balltype == shortLeg || balltype == shortStraight){
		event_tracker[dotBall] = true;
	}
	else if (balltype == fullStraight && shot_placement[spaceBar] && (shot_placement[leftArrow] || shot_placement[downArrow])){
		event_tracker[dotBall] = true;
		event_tracker[run] = true;
		event_tracker[fourRun] = true;		
		event_tracker[sixRun] = true;
	}
	else if(balltype == fullStraight || balltype == fullLeg){
		event_tracker[boldOut] = true;
	}
	else{
		event_tracker[dotBall] = true;
		event_tracker[catchOut] = true;
		event_tracker[boldOut] = true;
		event_tracker[run] = true;
		event_tracker[fourRun] = true;		
		event_tracker[sixRun] = true;
	}

	
	if(balltype == legWide || balltype == offWide){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[wideBall] = true;
	}
	if(shot_placement[spaceBar]){
		event_tracker[catchOut] = false;
		event_tracker[boldOut] = false;
	}


	
	if( (event_tracker[catchOut] || event_tracker[boldOut]) && this->ratingDifference() > -.2 ){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[catchOut] =  true;
		event_tracker[boldOut] = true;
	}
	else if( event_tracker[fourRun] && this->ratingDifference() <= 0 && this->ratingDifference() > -.5 ){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[fourRun] =  true;
	}
	else if( event_tracker[sixRun] && this->ratingDifference() <= -.5 ){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[sixRun] =  true;
	}
	else if( event_tracker[fourRun] && this->ratingDifference() > 0 && this->ratingDifference() < 1 ){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[run] =  true;
	}
	else if( event_tracker[sixRun] && this->ratingDifference() > 1 ){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[catchOut] =  true;
	}
	else if( event_tracker[run] ){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[run] =  true;
	}
	else if(event_tracker[leaveBall]){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[leaveBall] =  true;
	}
	else if(balltype == legWide || balltype == offWide){
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[wideBall] = true;
	}
	else{
		for(int i = 0 ; i < 9 ; i++){
			event_tracker[i] = false;
		}
		event_tracker[dotBall] =  true;
	}

	
	int i = 0, the_shot = 0;
	do{
		if(event_tracker[i]){
			the_shot = i;
			break;
		}
		i++;
	} while (!event_tracker[i] && i < 9);
	double temp = this->ratingDifference();
	return i;
}



double GameEngine::ratingDifference(){
	Team batting_side(rCricket->current_batting_team);
	Team bowling_side(rCricket->current_bowling_team);
	srand(time(NULL));
	return (bowling_side.players[rCricket->current_bowler].player_rating -  batting_side.players[rCricket->current_batsman].player_rating + (- 0.2 + (float)rand())/((float)RAND_MAX/(0.2 - .02)) );
}




/*

Key Reference: Here is the key value reference for shot_placement array

no shot = 0
up arrow = 1
right arrow = 2
down arrow = 3
left arrow = 4
space = 5

*/