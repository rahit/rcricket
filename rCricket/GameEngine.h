#pragma once
#include "Team.h"


class GameEngine{

private:

protected:
	Game *rCricket;

public:
	enum pitch{fullOff,fullStraight,fullLeg,goodOff,goodStraight,goodLeg,shortOff,shortStraight,shortLeg,legWide,offWide};
	enum possible_events{ dotBall, catchOut, boldOut, runOut, run, fourRun, sixRun, leaveBall, wideBall};
	enum fielding_setting_types{Aggressive,Moderate,Defensive};

	GameEngine(Game &);
	void bowl();
	void bat(int);
	void fielding_position(Screen &,int);
	int select_fielder(Screen &,int,int);
	int make_the_shot(int, bool a[]);
	double ratingDifference();
};

