#include "SDL.h"
#include "SDL_thread.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#include <set>
#include <map>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <cctype>
#include <cstdio>
#include <string>
#include <vector>
#include <random>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;


//Screen attributes
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;
const int SCREEN_BPP = 16;
const int MAX_SCREEN_ELEMENT = 100;
const int MAX_PLAYER = 11;
const int TOTAL_TEAM = 2;


template <class T>
string StringOf(T object){
	ostringstream os;
	os << object;
	return(os.str());
}


enum keys{ upArrow = 1 , rightArrow , downArrow , leftArrow , spaceBar };

struct background
{	
	SDL_Surface *location;
	int location_x;
	int location_y;
	int location_w;
	int location_h;
};

struct message
{	
	SDL_Surface *text;
	int text_x;
	int text_y;
	int text_w;
	int text_h;
};


struct Players{
	string player_name;
	int player_type;
	double player_rating;
};